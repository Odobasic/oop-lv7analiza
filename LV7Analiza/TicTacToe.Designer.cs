﻿namespace TicTacToe
{
    partial class TicTacToe
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.exit = new System.Windows.Forms.Button();
            this.label_turn = new System.Windows.Forms.Label();
            this.textBox_player1 = new System.Windows.Forms.TextBox();
            this.textBox_player2 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F);
            this.button1.Location = new System.Drawing.Point(14, 13);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(106, 90);
            this.button1.TabIndex = 0;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button_click);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F);
            this.button2.Location = new System.Drawing.Point(126, 13);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(106, 90);
            this.button2.TabIndex = 1;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button_click);
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F);
            this.button3.Location = new System.Drawing.Point(238, 13);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(106, 90);
            this.button3.TabIndex = 2;
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button_click);
            // 
            // button4
            // 
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F);
            this.button4.Location = new System.Drawing.Point(14, 109);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(106, 90);
            this.button4.TabIndex = 5;
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button_click);
            // 
            // button5
            // 
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F);
            this.button5.Location = new System.Drawing.Point(126, 109);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(106, 90);
            this.button5.TabIndex = 4;
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button_click);
            // 
            // button6
            // 
            this.button6.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F);
            this.button6.Location = new System.Drawing.Point(238, 109);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(106, 90);
            this.button6.TabIndex = 3;
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button_click);
            // 
            // button7
            // 
            this.button7.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F);
            this.button7.Location = new System.Drawing.Point(14, 205);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(106, 90);
            this.button7.TabIndex = 8;
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button_click);
            // 
            // button8
            // 
            this.button8.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F);
            this.button8.Location = new System.Drawing.Point(126, 205);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(106, 90);
            this.button8.TabIndex = 7;
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button_click);
            // 
            // button9
            // 
            this.button9.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F);
            this.button9.Location = new System.Drawing.Point(238, 205);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(106, 90);
            this.button9.TabIndex = 6;
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button_click);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(14, 393);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(122, 60);
            this.button10.TabIndex = 9;
            this.button10.Text = "Start";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button_start_Click);
            // 
            // exit
            // 
            this.exit.Location = new System.Drawing.Point(419, 389);
            this.exit.Name = "exit";
            this.exit.Size = new System.Drawing.Size(101, 64);
            this.exit.TabIndex = 11;
            this.exit.Text = "Exit";
            this.exit.UseVisualStyleBackColor = true;
            this.exit.Click += new System.EventHandler(this.button10_Click);
            // 
            // label_turn
            // 
            this.label_turn.AutoSize = true;
            this.label_turn.Location = new System.Drawing.Point(12, 325);
            this.label_turn.Name = "label_turn";
            this.label_turn.Size = new System.Drawing.Size(46, 17);
            this.label_turn.TabIndex = 12;
            this.label_turn.Text = "Turn: ";
            // 
            // textBox_player1
            // 
            this.textBox_player1.Location = new System.Drawing.Point(369, 205);
            this.textBox_player1.Name = "textBox_player1";
            this.textBox_player1.Size = new System.Drawing.Size(100, 22);
            this.textBox_player1.TabIndex = 14;
            // 
            // textBox_player2
            // 
            this.textBox_player2.Location = new System.Drawing.Point(369, 263);
            this.textBox_player2.Name = "textBox_player2";
            this.textBox_player2.Size = new System.Drawing.Size(100, 22);
            this.textBox_player2.TabIndex = 15;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(366, 185);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 17);
            this.label1.TabIndex = 16;
            this.label1.Text = "Player 1: ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(366, 243);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 17);
            this.label2.TabIndex = 17;
            this.label2.Text = "Player 2:";
            // 
            // TicTacToe
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(548, 479);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox_player2);
            this.Controls.Add(this.textBox_player1);
            this.Controls.Add(this.label_turn);
            this.Controls.Add(this.exit);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Name = "TicTacToe";
            this.Text = "TicTacToe";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button exit;
        private System.Windows.Forms.Label label_turn;
        private System.Windows.Forms.TextBox textBox_player1;
        private System.Windows.Forms.TextBox textBox_player2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}

